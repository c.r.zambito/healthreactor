from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import openai
from key import OPENAI_API_KEY
from ailearning import get_custom_response
import json

app = FastAPI()

openai.api_key = OPENAI_API_KEY
start_sequence = "\nA:"
restart_sequence = "\n\nQ: "


class Message(BaseModel):
    message: str


@app.post("/chat")
def chatbot(message: Message):
    custom_response = get_custom_response(message.message)

    if custom_response is not None: 
        answer = custom_response
        response_data = {"type": "custom", "answer": answer}
        print("Custom response JSON:", json.dumps(response_data, indent=2))
    else:
        prompt = f"{restart_sequence}{message.message}{start_sequence}"
        response = openai.Completion.create(
            engine="text-davinci-003",
            prompt=prompt,
            temperature=2.0,
            max_tokens=1024,
            top_p=1,
            frequency_penalty=0.5,
            presence_penalty=0.1
        )
        answer = response.choices[0].text.strip()
        response_data = {"type": "ai", "answer": answer, "response": response}
        print("AI-generated response JSON:", json.dumps(response_data, indent=2, default=str))

    return {"answer": answer}


origins = [
    "http://localhost",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)





# from fastapi import FastAPI
# from fastapi.middleware.cors import CORSMiddleware
# from pydantic import BaseModel
# import openai
# from key import OPENAI_API_KEY
# from ailearning import get_custom_response
# import json

# app = FastAPI()

# openai.api_key = OPENAI_API_KEY
# start_sequence = "\nA:"
# restart_sequence = "\n\nQ: "


# class Message(BaseModel):
#     message: str


# @app.post("/chat")
# def chatbot(message: Message):
#     custom_response = get_custom_response(message.message)

#     if custom_response:
#         answer = custom_response
#         response_data = {"type": "custom", "answer": answer}
#         print("Custom response JSON:", json.dumps(response_data, indent=2))
#     else:
#         prompt = f"{restart_sequence}{message.message}{start_sequence}"
#         response = openai.Completion.create(
#             engine="text-davinci-003",
#             prompt=prompt,
#             temperature=0.8,
#             max_tokens=1024,
#             top_p=1,
#             frequency_penalty=0.5,
#             presence_penalty=0.1
#         )
#         answer = response.choices[0].text.strip()
#         response_data = {"type": "ai", "answer": answer, "response": response}
#         print("AI-generated response JSON:", json.dumps(response_data, indent=2, default=str))

#     return {"answer": answer}


# origins = [
#     "http://localhost",
#     "http://localhost:3000",
# ]

# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=origins,
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )
