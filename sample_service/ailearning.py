import json
from fuzzywuzzy import process

with open("custom_responses.json", "r") as file:
    custom_response_categories = json.load(file)

custom_responses = {}
for category in custom_response_categories.values():
    custom_responses.update(category)

topics = ["brainstorming", "creativity", "innovation", "ideas", "football", "greetings"]


def get_custom_response(message: str) -> str:
    message = message.lower()

    best_match_key, best_match_score = process.extractOne(message, custom_responses.keys())

    similarity_threshold = 80

    if best_match_score >= similarity_threshold:
        return custom_responses[best_match_key]
    else:
        topic_found = any(topic in message for topic in topics)
        if topic_found:
            return None  # Return None when a topic is related but no custom response is available
        return "I'm sorry, I didn't understand your question. Can you please rephrase it? My main purpose is to help you with brainstorming, creativity, and innovation."



# import json
# from fuzzywuzzy import process

# with open("custom_responses.json", "r") as file:
#     custom_response_categories = json.load(file)

# custom_responses = {}
# for category in custom_response_categories.values():
#     custom_responses.update(category)

# topics = ["brainstorming", "creativity", "innovation", "ideas"]

# def get_custom_response(message: str) -> str:
#     message = message.lower()

#     best_match_key, best_match_score = process.extractOne(message, custom_responses.keys())

#     similarity_threshold = 80

#     if best_match_score >= similarity_threshold:
#         return custom_responses[best_match_key]
#     else:
#         for topic in topics:
#             if topic in message:
#                 return "I'm sorry, I don't have an answer to that question. Can you ask me something related to brainstorming, creativity, or innovation?"
#         return "I'm sorry, I didn't understand your question. Can you please rephrase it? My main purpose is to help you with brainstorming, creativity, and innovation."
