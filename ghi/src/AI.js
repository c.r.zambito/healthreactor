import React, { useState } from "react";
import axios from "axios";
import "./Chatbot.css";

function AI() {
    const [prompt, setPrompt] = useState("");
    const [idea, setIdea] = useState("");
    const [yesList, setYesList] = useState([]);
    const [noList, setNoList] = useState([]);
    const [maybeList, setMaybeList] = useState([]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const response = await axios.post("http://localhost:8000/chat", {
        message: prompt,
        });
        setIdea(response.data.answer);
    };

    const handleYes = () => {
        setYesList([...yesList, idea]);
        setIdea("");
    };

    const handleNo = () => {
        setNoList([...noList, idea]);
        setIdea("");
    };

    const handleMaybe = () => {
        setMaybeList([...maybeList, idea]);
        setIdea("");
    };

    const handleDelete = (listName, index) => {
        switch (listName) {
        case "yes":
            setYesList(yesList.filter((_, i) => i !== index));
            break;
        case "no":
            setNoList(noList.filter((_, i) => i !== index));
            break;
        case "maybe":
            setMaybeList(maybeList.filter((_, i) => i !== index));
            break;
        default:
            break;
        }
    };

    const handleDragStart = (e, listName, index) => {
        e.dataTransfer.setData("listName", listName);
        e.dataTransfer.setData("index", index);
    };

    const handleDragOver = (e) => {
        e.preventDefault();
    };

    const handleDrop = (e, listName) => {
        const sourceListName = e.dataTransfer.getData("listName");
        const sourceIndex = e.dataTransfer.getData("index");
        let sourceList, targetList, item;

        switch (sourceListName) {
        case "yes":
            sourceList = yesList;
            break;
        case "no":
            sourceList = noList;
            break;
        case "maybe":
            sourceList = maybeList;
            break;
        default:
            break;
        }

        switch (listName) {
        case "yes":
            targetList = yesList;
            break;
        case "no":
            targetList = noList;
            break;
        case "maybe":
            targetList = maybeList;
            break;
        default:
            break;
        }

        item = sourceList[sourceIndex];
        sourceList.splice(sourceIndex, 1);
        targetList.push(item);

        setYesList([...yesList]);
        setNoList([...noList]);
        setMaybeList([...maybeList]);
    };

    return (
        <div className="chatbot-container">
        <div className="chatbot-header">
            <h1>AI Chatbot</h1>
        </div>
        <div className="chatbot-body">
            <div className="chatbot-messages">
            {idea && (
                <div className="chatbot-message chatbot-ai-message">
                <p>{idea}</p>
                <div className="chatbot-buttons">
                    <button className="chatbot-yes-button" onClick={handleYes}>
                        YES
                    {/* <img src="yes-icon.png" alt="Yes" /> */}
                    </button>
                    <button className="chatbot-no-button" onClick={handleNo}>
                        NO
                    {/* <img src="no-icon.png" alt="No" /> */}
                    </button>
                    <button className="chatbot-maybe-button" onClick={handleMaybe}>
                        MAYBE???
                    {/* <img src="maybe-icon.png" alt="Maybe" /> */}
                    </button>
                </div>
                </div>
            )}
            </div>
            <form onSubmit={handleSubmit}>
            <div className="chatbot-input-container">
                <input
                type="text"
                className="chatbot-input"
                value={prompt}
                onChange={(e) => setPrompt(e.target.value)}
                placeholder="Type your message here..."
                />
                <button type="submit" className="chatbot-send-button">
                Send
                </button>
            </div>
            </form>
            <div className="chatbot-lists-container">
            <div className="chatbot-list-container">
                <h2>Yes</h2>
                <div
                className="chatbot-list chatbot-yes-list"
                onDragOver={handleDragOver}
                onDrop={(e) => handleDrop(e, "yes")}
                >
                {yesList.map((item, index) => (
                    <div
                    key={index}
                    className="chatbot-list-item"
                    draggable
                    onDragStart={(e) => handleDragStart(e, "yes", index)}
                    >
                    <p>{item}</p>
                    <button
                        className="chatbot-delete-button"
                        onClick={() => handleDelete("yes", index)}
                    >
                        X
                    </button>
                    </div>
                ))}
                </div>
            </div>
            <div className="chatbot-list-container">
                <h2>No</h2>
                <div
                className="chatbot-list chatbot-no-list"
                onDragOver={handleDragOver}
                onDrop={(e) => handleDrop(e, "no")}
                >
                {noList.map((item, index) => (
                    <div
                    key={index}
                    className="chatbot-list-item"
                    draggable
                    onDragStart={(e) => handleDragStart(e, "no", index)}
                    >
                    <p>{item}</p>
                    <button
                        className="chatbot-delete-button"
                        onClick={() => handleDelete("no", index)}
                    >
                        X
                    </button>
                    </div>
                ))}
                </div>
            </div>
            <div className="chatbot-list-container">
                <h2>Maybe</h2>
                <div
                className="chatbot-list chatbot-maybe-list"
                onDragOver={handleDragOver}
                onDrop={(e) => handleDrop(e, "maybe")}
                >
                {maybeList.map((item, index) => (
                    <div
                    key={index}
                    className="chatbot-list-item"
                    draggable
                    onDragStart={(e) => handleDragStart(e, "maybe", index)}
                    >
                    <p>{item}</p>
                    <button
                        className="chatbot-delete-button"
                        onClick={() => handleDelete("maybe", index)}
                    >
                        X
                    </button>
                    </div>
                ))}
                </div>
            </div>
            </div>
        </div>
        </div>
    );
}

export default AI;

// import React, { useState } from "react";
// import axios from "axios";
// import "./Chatbot.css";

// function AI() {
//     const [prompt, setPrompt] = useState("");
//     const [idea, setIdea] = useState("");
//     const [yesList, setYesList] = useState([]);
//     const [noList, setNoList] = useState([]);
//     const [maybeList, setMaybeList] = useState([]);

//     const handleSubmit = async (e) => {
//         e.preventDefault();
//         const response = await axios.post("http://localhost:8000/chat", {
//         message: prompt,
//         });
//         setIdea(response.data.answer);
//     };

//     const handleYes = () => {
//         setYesList([...yesList, idea]);
//         setIdea("");
//     };

//     const handleNo = () => {
//         setNoList([...noList, idea]);
//         setIdea("");
//     };

//     const handleMaybe = () => {
//         setMaybeList([...maybeList, idea]);
//         setIdea("");
//     };

//     const handleDelete = (listName, index) => {
//         switch (listName) {
//         case "yes":
//             setYesList(yesList.filter((_, i) => i !== index));
//             break;
//         case "no":
//             setNoList(noList.filter((_, i) => i !== index));
//             break;
//         case "maybe":
//             setMaybeList(maybeList.filter((_, i) => i !== index));
//             break;
//         default:
//             break;
//         }
//     };

//     const handleDragStart = (e, listName, index) => {
//         e.dataTransfer.setData("listName", listName);
//         e.dataTransfer.setData("index", index);
//     };

//     const handleDragOver = (e) => {
//         e.preventDefault();
//     };

//     const handleDrop = (e, listName) => {
//         const sourceListName = e.dataTransfer.getData("listName");
//         const sourceIndex = e.dataTransfer.getData("index");
//         let sourceList, targetList, item;

//         switch (sourceListName) {
//         case "yes":
//             sourceList = yesList;
//             break;
//         case "no":
//             sourceList = noList;
//             break;
//         case "maybe":
//             sourceList = maybeList;
//             break;
//         default:
//             break;
//         }

//         switch (listName) {
//         case "yes":
//             targetList = yesList;
//             break;
//         case "no":
//             targetList = noList;
//             break;
//         case "maybe":
//             targetList = maybeList;
//             break;
//         default:
//             break;
//         }

//         item = sourceList[sourceIndex];
//         sourceList.splice(sourceIndex, 1);
//         targetList.push(item);

//         setYesList([...yesList]);
//         setNoList([...noList]);
//         setMaybeList([...maybeList]);
//     };

//     return (
//         <div className="chatbot-container">
//         <div className="chatbot-header">
//             <h1>AI Chatbot</h1>
//         </div>
//         <div className="chatbot-body">
//             <div className="chatbot-messages">
//             {idea && (
//                 <div className="chatbot-message chatbot-ai-message">
//                 <p>{idea}</p>
//                 <div className="chatbot-buttons">
//                     <button className="chatbot-yes-button" onClick={handleYes}>
//                     Yes
//                     </button>
//                     <button className="chatbot-no-button" onClick={handleNo}>
//                     No
//                     </button>
//                     <button className="chatbot-maybe-button" onClick={handleMaybe}>
//                     Maybe
//                     </button>
//                 </div>
//                 </div>
//             )}
//             </div>
//             <form onSubmit={handleSubmit}>
//             <div className="chatbot-input-container">
//                 <input
//                 type="text"
//                 className="chatbot-input"
//                 value={prompt}
//                 onChange={(e) => setPrompt(e.target.value)}
//                 placeholder="Type your message here..."
//                 />
//                 <button type="submit" className="chatbot-send-button">
//                 Send
//                 </button>
//             </div>
//             </form>
//             <div className="chatbot-lists">
//             <div
//                 className="chatbot-list chatbot-yes-list"
//                 onDragOver={handleDragOver}
//                 onDrop={(e) => handleDrop(e, "yes")}
//             >
//                 <h2>Yes</h2>
//                 {yesList.map((item, index) => (
//                 <div
//                     key={index}
//                     className="chatbot-list-item"
//                     draggable
//                     onDragStart={(e) => handleDragStart(e, "yes", index)}
//                 >
//                     <p>{item}</p>
//                     <button
//                     className="chatbot-delete-button"
//                     onClick={() => handleDelete("yes", index)}
//                     >
//                     X
//                     </button>
//                 </div>
//                 ))}
//             </div>
//             <div
//                 className="chatbot-list chatbot-no-list"
//                 onDragOver={handleDragOver}
//                 onDrop={(e) => handleDrop(e, "no")}
//             >
//                 <h2>No</h2>
//                 {noList.map((item, index) => (
//                 <div
//                     key={index}
//                     className="chatbot-list-item"
//                     draggable
//                     onDragStart={(e) => handleDragStart(e, "no", index)}
//                 >
//                     <p>{item}</p>
//                     <button
//                     className="chatbot-delete-button"
//                     onClick={() => handleDelete("no", index)}
//                     >
//                     X
//                     </button>
//                 </div>
//                 ))}
//             </div>
//             <div
//                 className="chatbot-list chatbot-maybe-list"
//                 onDragOver={handleDragOver}
//                 onDrop={(e) => handleDrop(e, "maybe")}
//             >
//                 <h2>Maybe</h2>
//                 {maybeList.map((item, index) => (
//                 <div
//                     key={index}
//                     className="chatbot-list-item"
//                     draggable
//                     onDragStart={(e) => handleDragStart(e, "maybe", index)}
//                 >
//                     <p>{item}</p>
//                     <button
//                     className="chatbot-delete-button"
//                     onClick={() => handleDelete("maybe", index)}
//                     >
//                     X
//                     </button>
//                 </div>
//                 ))}
//             </div>
//             </div>
//         </div>
//         </div>
//     );
// }

// export default AI;

// import React, { useState } from "react";
// import axios from "axios";
// import "./Chatbot.css";

// function AI() {
//     const [prompt, setPrompt] = useState("");
//     const [idea, setIdea] = useState("");

//     const handleSubmit = async (e) => {
//         e.preventDefault();
//         const response = await axios.post("http://localhost:8000/chat", {
//         message: prompt,
//         });
//         setIdea(response.data.answer);
//     };

//     return (
//         <div className="chatbot-container">
//         <div className="chatbot-header">
//             <h1>AI Chatbot</h1>
//         </div>
//         <div className="chatbot-body">
//             <div className="chatbot-messages">
//             {idea && (
//                 <div className="chatbot-message chatbot-ai-message">
//                 <p>{idea}</p>
//                 </div>
//             )}
//             </div>
//             <form onSubmit={handleSubmit}>
//             <div className="chatbot-input-container">
//                 <input
//                 type="text"
//                 className="chatbot-input"
//                 value={prompt}
//                 onChange={(e) => setPrompt(e.target.value)}
//                 placeholder="Type your message here..."
//                 />
//                 <button type="submit" className="chatbot-send-button">
//                 Send
//                 </button>
//             </div>
//             </form>
//         </div>
//         </div>
//     );
// }

// export default AI;

// import React, { useState } from "react";
// import axios from "axios";

// function AI() {
//     const [prompt, setPrompt] = useState("");
//     const [idea, setIdea] = useState("");

//     const handleSubmit = async (e) => {
//         e.preventDefault();
//         const response = await axios.post("http://localhost:8000/chat", {
//         message: prompt,
//         });
//         setIdea(response.data.answer);
//     };

//     return (
//         <div>
//         <form onSubmit={handleSubmit}>
//             <label>
//             Prompt:
//             <input
//                 type="text"
//                 value={prompt}
//                 onChange={(e) => setPrompt(e.target.value)}
//             />
//             </label>
//             <button type="submit">Generate Idea</button>
//         </form>
//         {idea && <p>FuzzyWuzzy: {idea}</p>}
//         </div>
//     );
// }

// export default AI;
